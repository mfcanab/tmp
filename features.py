#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classification: UNCLASSIFIED

Created on Thu Oct 10 16:14:13 2019

"""

import numpy as np
import pandas as pd
import dateutil
import datetime
import glob
import logging

from io import PlatFile, plat_spectral_sense_enums, plat_mod_type

# Define logger
logger = logging.Logger(__name__)
formatter = logging.Formatter('%(asctime)s %(levelname)-5s %(message)s')
console_logger = logging.StreamHandler()
console_logger.setFormatter(formatter)
logger.addHandler(console_logger)
console_logger.setLevel(logging.DEBUG)

metric_scale = {'G': 1e9,
                'M': 1e6,
                'k': 1e3,
                'c': 1e-2,
                'm': 1e-3,
                'u': 1e-6,
                'n': 1e-9}


def append_files_in_dir(filename_out, path='*.epdw', verbose=False):
    # Get list of files to append
    files = list(glob.glob(path))
    files.sort()
    if filename_out in files:
        files = files.remove(filename_out)
    logger.info(f'Combining {len(files)} files.')

    # copy first file as "base" to build file on
    f_base = PlatFile(files[0])
    f_base.add_subr(calc_rf(f_base), 'Hz', name='FREQ_RF')
    f_base.write(filename_out, overwrite=True)

    for ifile in files[1:]:
        try:
            logger.debug(f'Operating on file : {ifile}')
            f = PlatFile(ifile)
            f.add_subr(calc_rf(f), 'Hz', name='FREQ_RF')
            f.write(filename_out, append=True)
        except:
            logger.error(f'Error combining {ifile}. Skipping')

def calc_rf(file, IF='PF', as_series=True, units='Hz'):
    '''
    Calculate RF from specified intermediate frequency IF and PlatFile

    Parameters
    ----------
    file: PlatFile
       PlatFile object the IF data is from
       this is needed to access the relevant header keywords to calculate RF
    IF: pandas.Series
    intermediate frequency

    '''
    if file.ehb.keyword_format.lower() == 'ascii':
        logger.warning('Header may not reflect RF changes accurately. Use RF at own risk.')

    # Grab intermediate frequency
    if isinstance(IF, str):
        IF = file.data[IF]

    # Initialize RF offset
    rf_offset = np.zeros(IF.shape)

    # Get spectral sense
    spectrum_inv = file.get_keyword('DATA_INVERSION_FLAG', 0)
    if isinstance(spectrum_inv, str):
        spectrum_inv = plat_spectral_sense_enums[spectrum_inv]

    # loop through each RF change event
    if type(file.get_keyword('COL_RF')) in (list, tuple):
        for i, i_offset in [(i, file.get_keyword('EVENT.OFFSET', 0)[i]) for i in file.get_keyword('COL_RF.EVENT')]:
            # Increment for some reason by 1 sample (matches ASPEN List tool)
            rf_offset[i_offset:] = file.get_keyword('COL_RF', 0)[file.get_keyword('COL_RF.EVENT').index(i)] + file.get_keyword('SBT_OFFSET', 0)
    else:
        rf_offset = file.get_keyword('COL_RF', 0) + file.get_keyword('SBT_OFFSET', 0)

    if as_series:
        return pd.Series(data=(rf_offset + (-1)**spectrum_inv*IF)*metric_scale.get(units[0], 1), name='RF')
    else:
        return (rf_offset + (-1)**spectrum_inv*IF)*metric_scale.get(units[0], 1)

def calc_abs_time(file, time='TOA', sample=None, sec_after_midnight=False, as_timedelta=False):
    '''
    Convert TOA or other specified subrecord from Counts to absolute time (HH:MM:SS)

    Parameters
    ----------
    file: PlatFile
       PlatFile object the IF data is from
    time: str or pandas.Series or pandas.DataFrame of int64 or float64 (default 'TOA')
        time to convert
        if time is a string, will attempt to access self.data[time]

    '''
    if round(file.hcb.type_code, -3) in (1000, 2000):
        if sample > file.num_elements:
            raise ValueError(f'Sample {sample} is greater than the number of records for the file')
        elif sample < 0:
            raise ValueError(f'Sample must be positive')

        sam = file.start_time + file.calc_file_time(sample=sample, as_timedelta=True)
        if sec_after_midnight and as_timedelta:
            return datetime.timedelta(hours=sam.hour, minutes=sam.minute, seconds=sam.second, microseconds=sam.microsecond)
        elif sec_after_midnight and not as_timedelta:
            return sam.hour*3600 + sam.minute*60 + sam.second + sam.microsecond*1e-6
        else:
            return file.start_time + file.calc_file_time(sample=sample, as_timedelta=True)

    elif round(file.hcb.type_code, -3) in (3000, 6000):

        if isinstance(time, str):
            name = time
            time = file.data[time]
        else:
            name = time.name

        if time.dtype == np.int64:

            if sec_after_midnight and as_timedelta:
                return pd.Series(data=pd.to_timedelta(np.mod(time, int(86400/file.ehb.keywords['TIME_DELTA']))//int(1e-9/file.ehb.keywords['TIME_DELTA'])), name=f'')
            elif sec_after_midnight and not as_timedelta:
                return pd.Series(data=np.mod(file.data['TOA'], int(86400/file.ehb.keywords['TIME_DELTA']))*file.ehb.keywords['TIME_DELTA'], name=f'{name}')
            elif not sec_after_midnight:
                return pd.Series(data=pd.to_datetime((time+5)//int(1e-9/file.ehb.keywords['TIME_DELTA']), origin=dateutil.parser.parse(file.ehb.keywords['TIME_EPOCH']).replace(tzinfo=None), utc=True), name=f'{name}')

        elif time.dtype == np.float64:

            if sec_after_midnight and as_timedelta and file.is_martes:
                return pd.Series(data=pd.to_timedelta(time,unit='s') + pd.to_timedelta(file.hcb.timecode % 86400, unit='s'), name=f'{name}')
            elif sec_after_midnight and as_timedelta and not file.is_martes:
                time_epoch = dateutil.parser.parse(file.ehb.keywords['TIME_EPOCH'])
                return pd.Series(data=pd.to_timedelta(time,unit='s') + pd.to_timedelta(time_epoch.hour*3600 + time_epoch.minute*60 + time_epoch.second, unit='s'), name=f'{name}')
            elif sec_after_midnight and not as_timedelta and file.is_martes:
                return pd.Series(data=time + file.hcb.timecode%86400, name=f'{name}')
            elif sec_after_midnight and not as_timedelta and not file.is_martes:
                time_epoch = dateutil.parser.parse(file.ehb.keywords['TIME_EPOCH'])
                return pd.Series(data=time + time_epoch.hour*3600 + time_epoch.minute*60 + time_epoch.second, name=f'{name}')
            elif not sec_after_midnight and file.is_martes:
                return pd.Series(data=pd.to_timedelta(time, unit='s') + pd.to_datetime(file.hcb.timecode, origin=datetime.datetime(1950,1,1).replace(tzinfo=None), utc=True, unit='s'), name=f'{name}')
            elif not sec_after_midnight and not file.is_martes:
                return pd.Series(data=pd.to_timedelta(time, unit='s') + pd.to_datetime(time, origin=dateutil.parser.parse(file.ehb.keywords['TIME_EPOCH']).replace(tzinfo=None), utc=True), name=f'{name}')

        else:
            raise ValueError(f'{name} must be int64 (counts) or float64 (seconds)')

def calc_file_time(file, time='TOA', sample=None, as_timedelta=False, as_series=True):
    '''
    Convert TOA or other specified subrecord from Counts to time relative to the start of the file

    Parameters
    ----------
    time: str or pandas.Series or pandas.DataFrame of int64 or float64 (default 'TOA')
        time to convert
        if time is a string, will attempt to access file.data[time]
    name: str (default 'TOA')
        name to assign output Series
    as_timedelta: bool (defult False)
        if True, return file time as a pandas timedelta object, otherwise, in seconds

    '''
    if sample is not None and sample > file.num_elements:
        raise ValueError(f'Sample {sample} is greater than the number of records for the file')
    elif sample is not None and sample < 0:
        raise ValueError(f'Sample must be positive')

    if round(file.hcb.type_code, -3) in (1000, ):
        if as_timedelta:
            return datetime.timedelta(seconds=file.hcb.x_delta*sample)
        else:
            return file.hcb.x_delta*sample

    elif round(file.hcb.type_code, -3) in (2000, ):
        if as_timedelta:
            return datetime.timedelta(seconds=file.hcb.y_delta*sample)
        else:
            return file.hcb.y_delta*sample

    elif round(file.hcb.type_code, -3) in (3000, 6000):

        if isinstance(time, str):
            name = time
            time = file.data[time]
        else:
            name = time.name

        if time.dtype == np.int64:
            # Truncate Times to nanosecond resolution so they can be converted between forms
            if as_timedelta and as_series:
                return pd.Series(data=pd.to_timedelta((time-time[0])//int(1e-9/file.ehb.keywords['TIME_DELTA'])), name=f'{name}')
            elif not as_timedelta and as_series:
                return pd.Series(data=(time-time[0])*file.ehb.keywords['TIME_DELTA'], name=f'{name}')
            elif as_timedelta and not as_series:
                return pd.to_timedelta((time-time[0])//int(1e-9/file.ehb.keywords['TIME_DELTA']))
            elif not as_timedelta and not as_series:
                return (time-time[0])*file.ehb.keywords['TIME_DELTA']


        # If floating point, already in File Time
        elif time.dtype == np.float64:
            if as_timedelta and as_series:
                return pd.Series(data=pd.to_timedelta(time,unit='s'), name=f'{name}')
            elif not as_timedelta and as_series:
                return pd.Series(data=time, name=f'{name}')
            elif as_timedelta and not as_series:
                return pd.to_timedelta(time,unit='s')
            elif not as_timedelta and not as_series:
                return time

        else:
            raise ValueError(f'{name} must be int64 (counts) or float64 (seconds)')

def calc_dtoa(file, time='TOA', as_timedelta=False, as_series=True):
    '''
    Calculate DTOA from TOA

    Parameters
    ----------
    time: str or pandas.Series or pandas.DataFrame of int64 or float64
        time to convert
        if time is a string, will attempt to access file.data[time]
    name: str (default 'DTOA')
       name to assign output Series

    '''
    if round(file.hcb.type_code, -3) not in (3000, 6000):
        raise AttributeError('calc_dtoa only works on parameterized files')

    if isinstance(time, str):
        name = time
        time = file.data[time]
    else:
        name = time.name

    if time.dtype == np.int64:
        # Truncate Times to nanosecond resolution so they can be converted between forms
        if as_timedelta and as_series:
            return pd.Series(data=pd.to_timedelta(np.append(np.diff(time),0)*file.ehb.keywords['TIME_DELTA']), name=f'D{name}')
        elif not as_timedelta and as_series:
            return pd.Series(data=np.append(np.diff(time),0)*file.ehb.keywords['TIME_DELTA'], name=f'D{name}')
        elif as_timedelta and not as_series:
            return pd.to_timdelta(np.append(np.diff(time),0)*file.ehb.keywords['TIME_DELTA'])
        elif not as_timedelta and not as_series:
            return np.append(np.diff(time),0)*file.ehb.keywords['TIME_DELTA']

    elif time.dtype in (np.float, np.float32, np.float64, float):
        if as_timedelta and as_series:
            return pd.Series(data=pd.to_timedelta(np.append(np.diff(time),0)), name=f'D{name}')
        elif not as_timedelta and as_series:
            return pd.Series(data=np.append(np.diff(time),0), name=f'D{name}')
        elif as_timedelta and not as_series:
            return pd.to_timdelta(np.append(np.diff(time),0))
        elif not as_timedelta and not as_series:
            return np.append(np.diff(time),0)

    else:
        raise ValueError(f'{name} must be int64 (counts) or float64 (seconds)')

def calc_bw(file, pw='PW', mod_rate='MOD_PARAMETER', mod_type='MOD_TYPE', as_series=True):
    '''
    Calculate bandwidth of the waveform record depending on modulatoin

    '''
    if isinstance(pw, str):
        pw = file.data[pw]

    if isinstance(mod_rate, str):
        mod_rate = file.data[mod_rate]

    if isinstance(mod_type, str):
        mod_type = file.data[mod_type]

    # initialize output array to 0
    bw = pw*0

    # Calculate LFM and PSK parameters and return
    iLFM = mod_type == plat_mod_type['LFM']
    bw[iLFM] = mod_rate[iLFM]*pw[iLFM]
    iPSK = mod_type == plat_mod_type['PSK']
    bw[iPSK] = mod_rate[iPSK]
    # DJ 31JAN2020, this may not be worth calculating, testing it out
    iUNMOD = mod_type == plat_mod_type['UNMODULATED']
    bw[iUNMOD] = 1/pw[iUNMOD]

    if as_series:
        return pd.Series(data=bw, name=f'BW')
    else:
        return bw

def calc_frame_time(file, time='TOA', frame_time=100e-3, as_timedelta=False, as_series=True):
    '''
    Calculate time into frame

    Parameters
    ----------
    time: str or pandas.Series or pandas.DataFrame of int64 or float64
        time to convert
        if time is a string, will attempt to access self.data[time]
    frame_time: float
        time in seconds to raster data

    '''

    if isinstance(time, str):
        name = time
        time = file.data[time]
    else:
        name = time.name

    if time.dtype == np.int64:
        # Truncate Times to nanosecond resolution so they can be converted between forms
        if as_timedelta and as_series:
            return pd.Series(data=pd.to_timedelta(np.mod(pd.to_timedelta((time-time[0])//int(1e-9/file.ehb.keywords['TIME_DELTA'])), frame_time)), name=f'FRAME_TIME')
        elif not as_timedelta and as_series:
            return pd.Series(data=np.mod((pd.to_timedelta((time-time[0])//int(1e-9/file.ehb.keywords['TIME_DELTA']))/pd.to_timedelta(1, unit='s')).values, frame_time), name=f'FRAME_TIME')
        elif as_timedelta and not as_series:
            return pd.to_timedelta(np.mod((time-time[0])//int(1e-9/file.ehb.keywords['TIME_DELTA']), frame_time))
        elif not as_timedelta and not as_series:
            return np.mod((time-time[0])//int(1e-9/file.ehb.keywords['TIME_DELTA']), frame_time)


    elif time.dtype in (np.float, np.float32, np.float64, float):

        if as_timedelta and as_series:
            return pd.Series(data=pd.to_timedelta(np.mod(time, frame_time)), name=f'FRAME_TIME')
        elif not as_timedelta and as_series:
            return pd.Series(data=np.mod(time, frame_time), name=f'FRAME_TIME')
        elif as_timedelta and not as_series:
            return pd.to_timedelta(np.mod(time, frame_time))
        elif not as_timedelta and not as_series:
            return np.mod(time, frame_time)


    else:
        raise ValueError(f'{name} must be int64 (counts) or float64 (seconds)')
