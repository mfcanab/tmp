# -*- coding: utf-8 -*-
"""
Classification : UNCLASSIFIED

This module can be used for reading and writing PLATINUN files.

Module supports 1000, 2000, 3000, and 6000 type files.

"""

# Import statements
import struct
import numpy as np
import os
import pandas as pd
import dateutil
import datetime
import sys
import psutil
import typing

__version__ = '0.0.11'


# Dictionary defininng machine format
mach_fmt = {'IEEE': '>', 'EEEI': '<'}
native_format = {'big': 'IEEE', 'little': 'EEEI'}

# Default 6003 subrecords
base_subr = ('TOA', 'PF', 'PW', 'PA', 'MOD_TYPE', 'MOD_PARAMETER')

# Platinum file extentions
file_ext = ('bdif', 'bpdw', 'cb', 'cdif', 'epdw', 'ext', 'fft', 'fil', 'mrb',
            'mrd', 'pdm', 'prm', 'rep', 'rfs', 'sdw', 'smp', 'tmi', 'tmp',
            'xpdw', 'xsdw')


class Keyword():
    '''
    Keyword class defing a keyword object for binary and ASCII formatted words

    Without any arguments, a blank Keyword object is returned.

    If key and value are specified, the keyword generating values are
    calculated for writing keywords
    '''
    def __init__(self, data: typing.Optional[dict]=None, fmt: str='binary'):

        # If initializing a new keyword
        if data is None:

            # Initialize for binary key representation
            if fmt.lower() == 'binary':
                self.next_offset = int(0)
                self.non_data_length = int(0)
                self.name_length = int(0)
                self.format_code = 'B'
                self.value = int(0)
                self.name = 'KEY'
                self.pad = int(0)

            # Initialize for ascii key representation
            elif fmt.lower() == 'ascii':
                self.key_offset = int(0)
                self.name_length = int(0)
                self.value_length = int(0)
                self.name = 'KEY'
                self.pad1 = int(0)
                self.value = int(0)
                self.pad2 = int(0)

        # If generating a keyword object from key and value
        elif data is not None:
            # make all keys upper case
            data = {k.upper():v for k,v in data.items()}
            # Only support creating keywords in binary representation
            self.name = list(data)[0].upper()
            self.value = data[self.name]
            self.name_length = len(list(data)[0])
            self.format_code = plat_format[np.array(self.value).dtype.char]

            # Calculate data length, pad length, and other parameters
            if isinstance(self.value, (str, list, tuple, np.ndarray)):
                data_len = len(self.value)*plat_type[self.format_code][1]
            else:
                data_len = plat_type[self.format_code][1]

            self.pad = int((8 - ((data_len + self.name_length) % 8)) % 8)
            self.next_offset = 8 + data_len + self.name_length + self.pad
            self.non_data_length = self.next_offset - data_len


class Subrecord():
    '''
    Subrecord class that defines single data field in 3000 or 6000 file
    If SUBREC_DEF is specified, Subrecord object parses the data
    If data is specified, the Subrecord object is updated to match

    Methods
    -------
    encode
        Encodes a data frame specification as
    '''
    def __init__(self, subrec):

        self.name = ''
        self.minval = int(0)
        self.maxval = int(0)
        self.offset = int(0)
        self.num_samps = int(0)
        self.units = int(31)
        self.format = 'SB'
        self.uprefix = '      '

        if isinstance(subrec, (bytearray, bytes, str)):

            # Type 3000 file
            if len(subrec) == 8:
                buff = struct.unpack('4s2sh', subrec)
                self.name = buff[0].replace(b'\x00', b'').decode().strip()
                self.format = buff[1].decode()
                self.offset = buff[2]

            elif len(subrec) == 96:
                self.name = subrec[00:24].strip()
                self.minval = float(subrec[24:48])
                self.maxval = float(subrec[48:72])
                self.offset = int(subrec[72:80])
                self.num_samps = int(subrec[80:84])
                self.units = int(subrec[84:88])
                self.format = subrec[88:90].strip()
                self.uprefix = subrec[90:96].strip()

        elif isinstance(subrec, dict):
            self.name = subrec['data'].name
            if not isinstance(subrec['data'][0], str):
                self.minval = min(subrec['data'])
                self.maxval = max(subrec['data'])
            self.num_samps = len(subrec['data'])
            if 'units' in subrec and isinstance(subrec['units'], str):
                self.units = plat_units[subrec['units']]
            elif 'units' in subrec and isinstance(subrec['units'], int):
                self.units = subrec['units']
            self.format = plat_rank[1] + plat_format[subrec['data'].dtype.char]

    def encode(subrec, codec=6000):
        '''
        Returns a string encoding information for a single subrecord for use
        in 6000 files based on a specifed data vector

        Parameters
        ----------
        data :
            Pandas DataFrame
        fmt encode_3000:
            format of data
        offset :
            offset of current data into record

        Returns
        -------
        str :
            SUBREC_DEF
        '''
        if codec == 3000:
            return struct.pack('4s2sh',
                               '{:<4}'.format(subrec['data'].name.upper())[:4].encode().replace(b' ', b'\x00'),
                               f'S{plat_format[subrec["data"].dtype.char]}'[:2].encode(),
                               subrec["offset"])
        elif codec == 6000:
            SUBREC_DEF = f'{subrec["data"].name.upper():<24}'[:24]
            if plat_format[subrec['data'].dtype.char] == 'A' or \
               len(f'{subrec["data"].min():+1.17e}') < 24 or \
               len(f'{subrec["data"].max():+1.17e}') < 24:
                SUBREC_DEF += '+0.00000000000000000e+00'
                SUBREC_DEF += '+0.00000000000000000e+00'
            else:
                SUBREC_DEF += f'{subrec["data"].min():+1.17e}'[:24]
                SUBREC_DEF += f'{subrec["data"].max():+1.17e}'[:24]
            SUBREC_DEF += f'{subrec["offset"]:<8d}'[:8]
            SUBREC_DEF += f'{1:<4d}'[:4]
            SUBREC_DEF += f'{subrec.get("units", int(31)):<4d}'[:4]
            SUBREC_DEF += f'S{plat_format[subrec["data"].dtype.char]}'[:2]
            SUBREC_DEF += f'      '

            return SUBREC_DEF


class HeaderControlBlock():
    '''
    This class represents the Header Control Block (HCB) that is made of the
    fixed and variable header blocks in the first 512 bytes of a Platinum file

    If data specified, HCB information will be generated appropriately

    Methods
    -------
    read
        Read HCB from particular file
    encode
        Encode HCB to bytes read for writing

    '''
    def __init__(self, data=None):

        # Fixed Header Control Block
        self.version = 'BLUE'
        self.head_rep = 'IEEE'
        self.data_rep = native_format[sys.byteorder]
        self.detached = int(0)
        self.protected = int(0)
        self.pipe = int(0)
        self.ext_start = int(0)
        self.ext_size = int(0)
        self.data_start = float(512)
        self.data_size = float(0)
        self.type_code = int(6000)
        self.format = 'NH'
        self.flagmask = int(0)
        self.timecode = float(0)
        self.inlet = int(0)
        self.outlets = int(0)
        self.outmask = int(0)
        self.pipeloc = int(0)
        self.pipesize = int(0)
        self.in_byte = float(0)
        self.out_byte = float(0)
        self.out_bytes = (0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
        self.keylength = int(96)
        self.keywords = {'VER': '2.0', 'IO': 'PLATINUM', 'CRC': '0'}

        self.tag_start = 0
        self.tag_delta = 1
        self.tag_units = 1
        self.x_start = 0
        self.x_delta = 1
        self.x_units = 1
        self.frame_length = 1
        self.y_start = 0
        self.y_delta = 1
        self.y_units = 1

        # If generating parameterized data
        if data is not None and isinstance(data, (pd.DataFrame, pd.Series)):

            self.type_code = 6003
            self.data_size = np.sum(pd.DataFrame(data).memory_usage()[1:])
            self.ext_start = int(np.ceil((self.data_start + self.data_size)/512))
            self.record_length = int(self.data_size//data.shape[0])

            try:
                self.num_subrecs = data.shape[1]
            except IndexError:
                self.num_subrecs = 1

        elif data is not None and (isinstance(data, np.ndarray) and len(data.shape) == 1):

            self.type_code = 1001

        elif data is not None and (isinstance(data, np.ndarray) and min(data.shape) > 1):

            self.type_code = 2000
            self.frame_length = data.shape[1]

    # File reading functions
    def read(self, filename):
        '''
        Reads Fixed Header Control Block

        Parameters
        ----------
        filename :
            path to file to read HCB from
        '''
        # Rewind to beginning of file
        with open(filename, 'rb') as fid:
            fid.seek(0)

            # This definition reads the fixed header and stores it
            fmt = struct.unpack('>4s4s4s', fid.read(12))
            if fmt[0].decode() not in ('BLUE', 'GOLD') or \
               fmt[1].decode() not in mach_fmt or \
               fmt[2].decode() not in mach_fmt:
                raise IOError(fid.name + ' not valid BLUE file.')

            # Read in proper endianess
            buff = struct.unpack(mach_fmt[fmt[1].decode()] + '5iddi2shdhh3idd8di92s', fid.read(244))

            # Parse fixed header
            self.version = fmt[0].decode()
            self.head_rep = fmt[1].decode()
            self.data_rep = fmt[2].decode()
            self.detached = int(buff[0])
            self.protected = int(buff[1])
            self.pipe = int(buff[2])
            self.ext_start = int(buff[3])
            self.ext_size = int(buff[4])
            self.data_start = float(buff[5])
            self.data_size = float(buff[6])
            self.type_code = int(buff[7])
            self.format = buff[8].decode()
            self.flagmask = int(buff[9])
            self.timecode = float(buff[10])
            self.inlet = int(buff[11])
            self.outlets = int(buff[12])
            self.outmask = int(buff[13])
            self.pipeloc = int(buff[14])
            self.pipesize = int(buff[15])
            self.in_byte = float(buff[16])
            self.out_byte = float(buff[17])
            self.out_bytes = buff[18:26]
            self.keylength = int(buff[26])
            self.keywords = {}
            temp_keywords = buff[27][:self.keylength].decode()

            # Parse keywords
            while '=' in temp_keywords:
                # Skip first character if a pad value
                if temp_keywords[0] == '\x00':
                    temp_keywords = temp_keywords[1:]
                    continue
                key = temp_keywords[:temp_keywords.find('\x00')].split('=')[0]
                val = temp_keywords[:temp_keywords.find('\x00')].split('=')[1]
                self.keywords[key] = val
                temp_keywords = temp_keywords[temp_keywords.find('\x00')+1:]

            '''
            Reads Variable Header Control Block
            '''
            # Rewind to beginning of variable header control block
            fid.seek(256)

            # Variable Header Control Block
            # 1000 Type one-dimensional data
            if round(self.type_code, -3) == 1000:
                # Read VHCB
                buff = struct.unpack(mach_fmt[self.head_rep] + 'ddi',
                                     fid.read(20))

                self.x_start = float(buff[0])
                self.x_delta = float(buff[1])
                self.x_units = int(buff[2])

            # 2000 Type two-dimensional data
            elif round(self.type_code, -3) == 2000:
                # Read VHCB
                buff = struct.unpack(mach_fmt[self.head_rep] + 'ddiiddi',
                                     fid.read(44))

                self.x_start = float(buff[0])
                self.x_delta = float(buff[1])
                self.x_units = int(buff[2])
                self.frame_length = int(buff[3])
                self.y_start = float(buff[4])
                self.y_delta = float(buff[5])
                self.y_units = int(buff[6])

            # 3000 Record structured data (obsoleted by 6000-type data)
            # 6000 Type Record data (non-homogeneous)
            elif round(self.type_code, -3) in (3000, 6000):
                # Read VHCB
                buff = struct.unpack(mach_fmt[self.head_rep] + 'ddiiddii208s',
                                     fid.read(256))
                self.tag_start = float(buff[0])
                self.tag_delta = float(buff[1])
                self.tag_units = int(buff[2])
                self.num_subrecs = int(buff[3])
                self.record_length = int(buff[7])
                self.subrecs = bytearray(buff[8][:8*self.num_subrecs])

                # Swap bytes if needed
                if native_format[sys.byteorder] != self.head_rep:
                    for i in list(range(min(self.num_subrecs, 26))):
                        self.subrecs[8*i+6], self.subrecs[8*i+7] = \
                         self.subrecs[8*i+7], self.subrecs[8*i+6]

        # Check if actual file size is smaller than reported size in header
        if os.stat(fid.name).st_size < 512*self.ext_start + self.ext_size:
            raise IOError(f'{fid.name} appears to be a partial, incomplete, or corrupt file. File size should be {512*self.ext_start + self.ext_size} bytes, but is only {os.stat(fid.name).st_size} bytes.')

    def encode(self):
        '''
        Encodes Header Control Block into binary

        Parameters
        print(out_filename)
        ----------
        data :
            data vector or pandas Data Frame

        Returns
        -------
        bytes :
            binary reprentation of HCB
        '''
        out_fmt = mach_fmt[native_format[sys.byteorder]]

        # prep keywords into a single string
        keywords = ''
        for key, val in self.keywords.items():
            keywords += key + '=' + val + chr(int(0))
        keylength = len(keywords)
        if round(self.type_code, -3) == 3000:
            keywords += (92-keylength)*chr(int(32))
        else:
            keywords += (92-keylength)*chr(int(0))


        # Generate fixed header block
        fhb_bytes = struct.pack(out_fmt + '4s4s4s5i2di2shd2h3i2d8di92s',
                                'BLUE'.encode(),
                                native_format[sys.byteorder].encode(),
                                native_format[sys.byteorder].encode(),
                                self.detached,
                                self.protected,
                                self.pipe,
                                self.ext_start,
                                self.ext_size,
                                self.data_start,
                                self.data_size,
                                self.type_code,
                                self.format.encode(),
                                self.flagmask,
                                self.timecode,
                                self.inlet,
                                self.outlets,
                                self.outmask,
                                self.pipeloc,
                                self.pipesize,
                                self.in_byte,
                                self.out_byte,
                                self.out_bytes[0],
                                self.out_bytes[1],
                                self.out_bytes[2],
                                self.out_bytes[3],
                                self.out_bytes[4],
                                self.out_bytes[5],
                                self.out_bytes[6],
                                self.out_bytes[7],
                                keylength,
                                keywords.encode())

        if round(self.type_code, -3) == 1000:
            vhb_bytes = struct.pack(out_fmt + 'ddi236x',
                                    self.x_start,
                                    self.x_delta,
                                    self.x_units)
        elif round(self.type_code, -3) == 2000:
            vhb_bytes = struct.pack(out_fmt + 'ddiiddi212x',
                                    self.x_start,
                                    self.x_delta,
                                    self.x_units,
                                    self.frame_length,
                                    self.y_start,
                                    self.y_delta,
                                    self.y_units)
        elif round(self.type_code, -3) in (3000, 6000):
            vhb_bytes = struct.pack(out_fmt + 'ddiiddii208s',
                                    self.tag_start,
                                    self.tag_delta,
                                    self.tag_units,
                                    self.num_subrecs,
                                    0,
                                    self.tag_delta,
                                    self.tag_units,
                                    self.record_length,
                                    self.subrecs)

        return bytes().join([fhb_bytes, vhb_bytes])


class ExtendedHeaderBlock():
    '''
    This class represents the Extened Header Block (EHB) that is made of the
    keywords at the end of a Platinum file

    Methods
    -------
    read
        Read EHB from particular file
    encode
        Encode EHB to bytes read for writing

    '''
    def __init__(self):
        self.keywords = {'ACQDATE': '19700101',
                         'ACQTIME': '00:00:00',
                         'EVENT.DURATION': 0.0,
                         'EVENT.OFFSET': 0,
                         'EVENT.TIME': 0,
                         'TIME_DELTA': 1e-10,
                         'TIME_EPOCH': '1970-01-01T00:00:00Z',
                         'TIMETAG.OFFSET': 0,
                         'TIMETAG.TIME': 0}
        self.keyword_format = 'Binary'

    def read(self, filename, hcb):
        '''
        Reads Extended Header Block given file identifier and HCB

        Parameters
        ----------
        filename :
            path to file read EHB from
        hcb :
            HeaderControlBlock object
        '''
        # If no extended header, return nothing
        if hcb.ext_size == 0 or hcb.ext_start == 0:
            return

        self.num_keywords = 0

        # Get keyword format
        # Seek to start of extended header block
        with open(filename, 'rb') as fid:
            fid.seek(512*hcb.ext_start)

            # Attemp to read as ASCII format
            buff = struct.unpack(mach_fmt[hcb.head_rep] + 'iii', fid.read(12))
            next_offset = buff[0]
            name_length = buff[1]
            data_length = buff[2]
            pad1 = int(4*np.ceil(float(name_length)/4)-name_length)
            pad2 = int(4*np.ceil(float(data_length)/4)-data_length)
            expected_length = name_length + data_length + pad1 + pad2 + 12

            # ASCII format
            if name_length < 65535 and next_offset == expected_length:
                self.keyword_format = 'ASCII'
            # Binary format
            else:
                self.keyword_format = 'Binary'

            fid.seek(512*hcb.ext_start)

            try:

                # Parse extended header
                if self.keyword_format == 'Binary':

                    # Loop through extended header
                    while (fid.tell() < (hcb.ext_start*512 + hcb.ext_size)):

                        # Initialize current keyword
                        i_key = Keyword(fmt=self.keyword_format)

                        # Extract keyword storage information
                        keyInfo = struct.unpack(mach_fmt[hcb.head_rep] + 'ihbs', fid.read(8))
                        i_key.next_offset = keyInfo[0]
                        i_key.non_data_length = keyInfo[1]
                        i_key.name_length = keyInfo[2]
                        i_key.format_code = keyInfo[3].decode()

                        # find data format type
                        keyformat = plat_type[i_key.format_code]

                        # read data
                        bytes_to_read = (i_key.next_offset-i_key.non_data_length)//keyformat[1]
                        i_key.value = list(struct.unpack(mach_fmt[hcb.head_rep] + str(bytes_to_read)+keyformat[0], fid.read(bytes_to_read*keyformat[1])))

                        if i_key.format_code == 'A':
                            i_key.value = i_key.value[0].decode()
                        else:
                            # force numeric data to proper format
                            i_key.value = [keyformat[2](i) for i in i_key.value]

                        i_key.pad = i_key.non_data_length-i_key.name_length-8  # null padding between this record and next
                        i_key.name = struct.unpack(mach_fmt[hcb.head_rep] + str(i_key.name_length)+'s'+i_key.pad*'x', fid.read(i_key.name_length+i_key.pad))[0].decode()

                        # If there is only 1 item in the data tuple, take it out of the tuple
                        if len(i_key.value) == 1:
                            i_key.value = i_key.value[0]

                        # Record keyword
                        self.keywords[i_key.name] = i_key.value

                        # Increment keyword counter
                        self.num_keywords += 1

                elif self.keyword_format.lower() == 'ascii':

                    totOffset = 0

                    # Loop through extended header
                    while (fid.tell() < (hcb.ext_start*512 + hcb.ext_size)):

                        # Initialize current keyword
                        i_key = Keyword(fmt=self.keyword_format)

                        # Extract keyword storage information
                        keyInfo = struct.unpack(mach_fmt[hcb.head_rep] + 'iii', fid.read(12))
                        i_key.key_offset = keyInfo[0]
                        i_key.name_length = keyInfo[1]
                        i_key.value_length = keyInfo[2]

                        # Calcualate pad values
                        i_key.pad1 = int((4 - (i_key.name_length % 4)) % 4)
                        i_key.pad2 = i_key.key_offset - totOffset - 12 - i_key.name_length - i_key.pad1 - i_key.value_length


                        # Read key name
                        i_key.name = struct.unpack(str(i_key.name_length) + 's' + i_key.pad1*'x', fid.read(i_key.name_length + i_key.pad1))[0].decode()

                        # Read key value
                        i_key.value = struct.unpack(str(i_key.value_length) + 's' + i_key.pad2*'x', fid.read(i_key.value_length + i_key.pad2))[0].decode()
                        # attempt to convert to number
                        try:
                            i_key.value = int(i_key.value)
                        except ValueError:
                            try:
                                i_key.value = float(i_key.value)
                            except ValueError:
                                pass

                        totOffset = i_key.key_offset

                        # Record keyword
                        self.keywords[i_key.name] = i_key.value

                        # Increment keyword counter
                        self.num_keywords += 1

            except ValueError:
                raise IOError('Error reading extended header block')

    def encode(self):
        '''
        Encodes Extended Header Block into byte represenetation

        Returns
        ----------
        bytes :
            Extended Header Block Representation
        '''
        out_fmt = mach_fmt[native_format[sys.byteorder]]

        keyword_objects = []
        for key in sorted(self.keywords.keys()):
            keyword_objects.append(Keyword(data={key: self.keywords[key]},
                                           fmt=self.keyword_format))

        binary = bytes()
        for i_key in keyword_objects:

            # Set pad value to pad until end of current 512 byte block for last keyword
            if i_key == keyword_objects[-1]:
                extra_pad = int(np.ceil((len(binary)+i_key.next_offset)/512)*512-(len(binary)+i_key.next_offset))
                i_key.non_data_length = i_key.non_data_length + extra_pad
                i_key.next_offset = i_key.next_offset + extra_pad
                i_key.pad = i_key.pad + extra_pad

            if i_key.format_code == 'A':
                value = i_key.value.encode()
            else:
                value = i_key.value

            # Encode Keyword header
            binary = bytes().join([binary, struct.pack(out_fmt + 'ihbs', i_key.next_offset, i_key.non_data_length, i_key.name_length, i_key.format_code.encode())])

            # Encode values
            # for a vector of values
            if type(value) in (list, tuple):
                for i_val in value:
                    binary = bytes().join([binary, struct.pack(out_fmt + plat_type[i_key.format_code][0], i_val)])
            # for a string value
            elif i_key.format_code == 'A':
                binary = bytes().join([binary, struct.pack(out_fmt + str(len(value)) + plat_type[i_key.format_code][0], value)])
            # for scalar value
            else:
                binary = bytes().join([binary, struct.pack(out_fmt + plat_type[i_key.format_code][0], value)])

            # Encode name and pads
            binary = bytes().join([binary, struct.pack(str(i_key.name_length) + 's' + str(i_key.pad) + 'x', i_key.name.encode())])

        return binary


class TagSet():
    '''
    Support for ASPEN tag set

    Methods
    -------
    gen_tag_set
        Generate a tag set with fully formed tag descriptions for ASPEN
    set_tag_color
        Specify the tag color for a paticular tag set
    set_tag_name
        Specify the name for a paticular tag set
    '''

    def __init__(self, data=None, tag_set_string=None):
        self.tags = []

        # parse exisitng tag set
        if tag_set_string is not None:
            tag_sets = [x for x in tag_set_string.split('\n') if len(x) > 0]
            parsed_tags = []
            for i_tag in tag_sets:
                tag_string_split = i_tag.split(':')
                parsed_tags.append({'name': tag_string_split[2].strip(),
                                    'value': tag_string_split[0],
                                    'color': tag_string_split[1]})

        # generate tag set from data
        if data is not None:
            # generate new tags
            new_tags = TagSet.gen_tag_set(data=data)
            if [x['name'] for x in new_tags].count('UNTAGGED') == 0:
                new_tags.append({'name': 'UNTAGGED',
                                 'value': '        ',
                                 'color': '#99ccff'})

        if data is None and tag_set_string is not None:
            self.tags = parsed_tags

        elif data is not None and tag_set_string is None:
            self.tags = new_tags

        elif data is not None and tag_set_string is not None:

            # inititialize output tags with existing parsed tag set
            combined_tags = parsed_tags
            parsed_tag_names = [x['name'] for x in parsed_tags]
            new_tag_names = [x['name'] for x in new_tags]
            # loop over the tags generated from the data
            # if the tag doesnt exist in the
            for itag in new_tag_names:
                if itag not in parsed_tag_names:
                    new_tags[new_tag_names.index(itag)]['color'] = aspen_color_pallette[len(combined_tags) % len(aspen_color_pallette)]
                    combined_tags.append(new_tags[new_tag_names.index(itag)])

            self.tags = combined_tags

    def gen_tag_set(data):
        # get unique tags
        unique_labels = data.unique()

        # create list of truncated tags for comparison
        trunc_labels = [x[:5] for x in unique_labels]

        tags = []
        for i in enumerate(unique_labels):

            tag = dict()
            tag['name'] = i[1].strip()
            if len(tag['name']) > 8:
                tag['value'] = f'{trunc_labels[i[0]]}{trunc_labels[:i[0]].count(trunc_labels[i[0]]):>3d}'.replace(' ', '~').upper()
            else:
                tag['value'] = f'{tag["name"]:<8s}'[:8]
            tag['color'] = f'{aspen_color_pallette[i[0]%len(aspen_color_pallette)]}'
            tags.append(tag)

        return tags

    def set_tag_color(self, tag_name, color):
        self.tags[self.get_tag_index(tag_name)]['color'] = color

    def set_tag_name(self, tag_name, name):
        self.tags[self.get_tag_index(tag_name)]['name'] = name

    @property
    def ntags(self):
        return len(self.tags)

    @property
    def tag_set_string(self):
        tag_set_string = ''
        for itag in self.tags:
            tag_set_string += f'{itag["value"]}:{itag["color"]}:{itag["name"]}\n'
        return tag_set_string

    @property
    def value_string(self):
        value_string = ''
        for itag in self.tags:
            value_string += itag['value']
        return value_string

    @property
    def tag_names(self):
        return [x['name'] for x in self.tags]

    @property
    def tag_values(self):
        return [x['value'] for x in self.tags]

    @property
    def tag_colors(self):
        return [x['color'] for x in self.tags]

    def get_tag_index(self, tag):
        if tag in self.tag_names:
            return self.tag_names.index(tag)
        elif tag in self.tag_values:
            return self.tag_values.index(tag)
        else:
            raise ValueError(f'\'{tag}\' is not in tag names or values')


class PlatFile():
    '''
    PlatFile class represents a Platinum or Martes file

    PlatFile contains a HeaderControlBlock object, which conatins the HCB data,
    a DataBlock object containing data in the file, and an ExtendedHeaderBlock
    that contains keywords

    On initialization, if data is specified

    '''

    def __init__(self, filename=None, read_data=True, subr_to_read=None,
                       data=None, start_element=0, pipe_size=None, new=False):
        '''
        Parameters
        ----------
        filename : str
            filename to PLATINUM file to be read or written
        read_data : bool (default True)
            read data on instantiation of PlatFile
        subr_to_read: list
            list of subrecords to read froma 3000 or 6000 type file
        data : np.array, np.ndarray, pd.DataFrame (defualt None)
            data used to create a new Platinum file
        pipe_size: int
            size in bytes to read when iterating over a file

        '''

        # Initialize Structure
        if filename is not None:
            self.filename = filename
        else:
            self.filename = []
        self.hcb = HeaderControlBlock()
        self.data = []
        self.ehb = ExtendedHeaderBlock()
        self.data_element_head = int(0)
        self.data_element_tail = int(0)
        self.atom_size = 0
        self.element_size = 0
        self.num_elements = int(0)
        self.dt = None
        self.pipe_size = None

        # If reading existing file
        if filename is not None and not new:
            # Read header control block
            self.hcb.read(self.filename)

            # Parse SUBREC_DEF if given for 3000 type file
            if round(self.hcb.type_code, -3) == 3000:
                self.subrecs = []
                for i in list(range(len(self.hcb.subrecs)//8)):
                    self.subrecs.append(Subrecord(self.hcb.subrecs[8*i+0:8*i+8]))

            # Read Extended Header Block
            self.ehb.read(self.filename, self.hcb)

            # Parse SUBREC_DEF if given for 6000 type file
            if round(self.hcb.type_code, -3) == 6000:
                self.subrecs = []
                for i in list(range(len(self.ehb.keywords['SUBREC_DEF'])//96)):
                    self.subrecs.append(Subrecord(self.ehb.keywords['SUBREC_DEF'][96*i+0:96*i+96]))

            # Get information about data size
            if round(self.hcb.type_code, -3) in (3000, 6000):
                self.atom_size = None
                self.element_size = self.hcb.record_length

                if isinstance(subr_to_read, str):
                    subr_to_read = (subr_to_read, )

                # Create a list of data type formats
                type_fmt = dict(names=[], formats=[], offsets=[], itemsize=0)
                for s in self.subrecs:

                    if subr_to_read is not None and s.name not in subr_to_read:
                        continue

                    # Leave alone if only single entry for each subrecord
                    if plat_rank[s.format[0]] == 1 and s.format[1] != 'A':

                        type_fmt['names'].append(s.name)
                        type_fmt['formats'].append(plat_type[s.format[1]][2])
                        type_fmt['offsets'].append(s.offset)

                    # If multiple entries in each subrecord, separate into individual subrecords
                    elif plat_rank[s.format[0]] == 1 and s.format[1] == 'A':
                        for i in range(plat_rank[s.format[0]]):
                            type_fmt['names'].append(s.name)
                            type_fmt['formats'].append('S8')
                            type_fmt['offsets'].append(s.offset)
                    elif plat_rank[s.format[0]] != 1:
                        for i in range(plat_rank[s.format[0]]):
                            type_fmt['names'].append(s.name+str(i))
                            type_fmt['formats'].append(plat_type[s.format[1]][2])
                            type_fmt['offsets'].append(s.offset+plat_type[s.format[1]][1]*i)

                type_fmt['itemsize'] = self.hcb.record_length

                # Create custom data type
                self.dt = np.dtype(type_fmt).newbyteorder(mach_fmt[self.hcb.data_rep])

                if self.dt.itemsize != self.hcb.record_length:
                    raise IOError('Error: Interpreted data record length ({}) does not match header record length ({})'.format(self.dt.itemsize, self.hcb.record_length))

                # Extract tags if present
                self.tags = dict()
                for tag_set in [x.split('.')[0] for x in self.ehb.keywords.keys() if x.endswith('.TAGS')]:
                    self.tags[f'{tag_set}'] = TagSet(tag_set_string=self.get_keyword(f'{tag_set}.TAGS'))
            else:
                self.dt = np.dtype(plat_type[self.hcb.format[1]][2]).newbyteorder(mach_fmt[self.hcb.data_rep])
                self.atom_size = self.dt.itemsize
                self.element_size = plat_rank[self.hcb.format[0]]*self.atom_size

            self.num_elements = int(self.hcb.data_size//self.element_size)

            # Read data from assicaited 6000 file
            if self.hcb.type_code == 1004:
                self.assoc6000 = PlatFile(os.path.join(os.path.split(self.filename)[0], self.get_keyword('ASSOC6000')))

            # Read data if desired
            if pipe_size is not None:
                self.pipe_size = int(pipe_size)

            if read_data and ((round(self.hcb.type_code, -3) in (1000, 2000) and pipe_size is not None) or round(self.hcb.type_code, -3) in (3000, 6000)):
                self.read(start_element=start_element)

        # Data is being provided to create a new file
        elif data is not None:

            if isinstance(data, pd.Series):
                self.data = pd.DataFrame(data)
                self.hcb.type_code = int(6003)
            elif isinstance(data, pd.DataFrame):
                self.data = data
                self.hcb.type_code = int(6003)
            elif isinstance(data, np.ndarray):
                self.data = data
                self.hcb.type_code = int(1001)
            elif isinstance(data, dict):
                self.data = pd.DataFrame.from_dict(data)
                self.hcb.type_code = int(6003)
            else:
                raise ValueError('Data must be given in a dict, Pandas Series, DataFrame, numpy array or ndarray')

            # populate records
            if round(self.hcb.type_code, -3) in (3000, 6000):
                self.subrecs = []
                for i in self.data.columns:
                    self.subrecs.append(Subrecord({'data': self.data[i]}))

            # Update header
            self.update_header()

    def __iter__(self):
        return self

    def __next__(self):
        if self.hcb.type_code == 1004 and self.data_element_tail >= self.assoc6000.num_elements:
            raise StopIteration
        elif round(self.hcb.type_code,-3) == 2000 and self.data_element_tail >= int(self.num_elements//self.hcb.frame_length):
            raise StopIteration
        elif round(self.hcb.type_code,-3) in (1000, 6000) and self.data_element_tail >= self.num_elements:
            raise StopIteration

        self.read(start_element=self.data_element_tail, num_elements=self.pipe_size)

        return self.data

    def reset_iter(self):
        self.data_element_head = int(0)
        self.data_element_tail = int(0)

## TO DO
## specify time to read in seconds
## specify start time vice start element

    def read(self, start_element=0, num_elements=-1):
        '''
        Reads data from PLAINTUM file

        Parameters
        ----------
        start_element : int (default 0)
            specifies which element to start reading data from
            For 1000 and 2000 type files, this is the sample
            For 1004 type files, this is the gate
            For 2000 type files, this is the frame
            For 3000 and 6000 type files, this is the row/record
        num_elements : int (default -1)
            specifies the number of elements to read
            -1 indicates the entire file should be read
            if reading as frames, one element will be one frame
        max_data_read_gb : int, float (default 4)
            Specifies the max number or bytes to read when reading 1000 type files
        '''

        if num_elements == 0:
            self.data = []
            return
        elif num_elements > 0:
            self.pipe_size = int(num_elements)
        elif num_elements == -1 and self.pipe_size is None:
            if self.hcb.type_code == 1004:
                self.pipe_size = int(self.assoc6000.data.shape[0])
            elif round(self.hcb.type_code,-3) == 2000:
                self.pipe_size = int(self.num_elements//self.hcb.frame_length)
            else:
                self.pipe_size = int(self.num_elements)

        self.data_element_head = int(start_element)
        self.data_element_tail = self.data_element_head + self.pipe_size

        # Determine number of elements to read
        if self.hcb.type_code == 1004:
            self.data_element_tail = int(min([self.data_element_tail, self.assoc6000.num_elements]))

        elif round(self.hcb.type_code,-3) == 2000:
            self.data_element_tail = int(min([self.data_element_tail, self.num_elements//self.hcb.frame_length]))

        elif round(self.hcb.type_code,-3) in (1000, 6000):
            self.data_element_tail = int(min([self.data_element_tail, self.num_elements]))

        # Limit the amount of data that can be read to 75% of availible memory or 2 GB
        if self.element_size*(self.data_element_tail-self.data_element_head) > max([int(psutil.virtual_memory().available*1024*0.75), 2e9]):
            raise MemoryError(f'Attempted to read too much data. Limiting max data size to {max([int(psutil.virtual_memory().available*1024*0.75), 2e9])/1024**3} due to memory constraints.')

        with open(self.filename, 'rb') as fid:

            if self.hcb.type_code == 1004:

                #fid.seek(int(self.hcb.data_start + self.assoc6000.data['ADDRESS'][self.data_element_head]-1))
                fid.seek(int(self.hcb.data_start + self.assoc6000.data['ADDRESS'][self.data_element_head]*self.element_size))
                self.data = np.fromfile(fid, dtype=self.dt, count=plat_rank[self.hcb.format[0]]*int(sum(self.assoc6000.data['NUMBER_SAMPLES'][self.data_element_head:self.data_element_tail])))

                if self.hcb.format[0] == 'C':
                    self.data = self.data[0::2] + 1j*self.data[1::2]

            # Type two-dimensional data
            # Frame - Platinum uses this exclusively to represent spectral data iwth frequeny on the x-axis and time on the y-axis. Martes and X-Midas may allow other uses.
            elif round(self.hcb.type_code,-3) == 2000:

                fid.seek(int(self.hcb.data_start + self.element_size*self.data_element_head*self.hcb.frame_length))
                self.data = np.fromfile(fid, dtype=self.dt, count=int(plat_rank[self.hcb.format[0]]*(self.data_element_tail-self.data_element_head)*self.hcb.frame_length))

                if self.hcb.format[0] == 'C':
                    self.data = self.data[0::2] + 1j*self.data[1::2]

                # Reshape to size framed data
                self.data = self.data.reshape((self.hcb.frame_length, self.data.size//self.hcb.frame_length), order='F')

            elif round(self.hcb.type_code,-3) == 1000:

                # seek to appropriate spot in file if elements are actually elements
                fid.seek(int(self.hcb.data_start + self.element_size*self.data_element_head))
                self.data = np.fromfile(fid, dtype=self.dt, count=plat_rank[self.hcb.format[0]]*(self.data_element_tail-self.data_element_head))

                if self.hcb.format[0] == 'C':
                    self.data = self.data[0::2] + 1j*self.data[1::2]

            # 6000 Type Record Data (non-homogeneous)
            # Uniformly-Sampled - Uniformly sampled record data is not part of the Platinum standard
            elif round(self.hcb.type_code,-3) in (3000, 6000):

                fid.seek(int(self.hcb.data_start + self.hcb.record_length*self.data_element_head))

                # Read with numpy into pandas DataFrame
                # pandas looks for byte order in little-endian form, swap bytes if needed
                if self.hcb.data_rep == native_format[sys.byteorder]:
                    self.data = pd.DataFrame(np.fromfile(fid, dtype=self.dt, count=self.data_element_tail-self.data_element_head))
                else:
                    self.data = pd.DataFrame(np.fromfile(fid, dtype=self.dt, count=self.data_element_tail-self.data_element_head).byteswap().newbyteorder())

                # Decode any ASCII fields from bytes to strings
                # if there is an aspen tag set that has field names that are longer than the 8 char limit, reassign to full value
                for i in range(len(self.dt)):
                    if self.dt[i].char == 'S':
                        self.data[self.dt.names[i]] = self.data[self.dt.names[i]].str.decode('UTF-8')

                        tag_set_string = self.get_keyword(f"{self.dt.names[i]}.TAGS")
                        if not tag_set_string:
                            self.tags[self.dt.names[i]] = TagSet(data=self.data[self.dt.names[i]])
                            aspen_tags = self.tags[self.dt.names[i]].tags
                        else:
                            aspen_tags = TagSet(tag_set_string=tag_set_string).tags

                        for x in aspen_tags:
                            self.data.loc[self.data[self.dt.names[i]] == x['value'], self.dt.names[i]] = x['name']

    def write(self, out_filename=None, append=False, overwrite=False):
        '''
        Writes currently defined PlatFile object to disk

        Parameters
        ----------
        out_filename: str
            file name of output file to be written
        append: bool (default False)
            False for writing to a new file or overwriting an existing file
            True for appending to a file
        overwrite: bool (default False)
            whether to overwrite an existing file
        '''
        mode = 'wb'

        # If no file name presented, use own filename
        if out_filename is None:
            out_filename = self.filename

        # Check if file exists
        if os.path.exists(out_filename) and not (overwrite or append):
            raise IOError(f'{out_filename} already exists. Set overwrite=True or choose new filename.')

        # Update header data just in case
        self.update_header()

        if append and not os.path.exists(out_filename):
            append = False

        if append:

            mode = 'r+b'

            base_file = PlatFile(out_filename, read_data=False)

            # Ensure file types are consistent
            if self.hcb.type_code != base_file.hcb.type_code:
                raise ValueError(f'File type mismatch. Cannot append {self.hcb.type_code} and {base_file.hcb.type_code} type files.')
            if self.hcb.format != base_file.hcb.format:
                raise ValueError(f'File format mismatch. Cannot append {self.hcb.format} and {base_file.hcb.format} format files.')

            # Update header data just in case
            orig_size = base_file.hcb.data_size
            base_file.update_header(append_data_len=len(self.data))

        with open(out_filename, mode) as fid:

            fid.seek(0)
            if append:
                fid.write(base_file.hcb.encode())
                fid.seek(int(base_file.hcb.data_start+orig_size))
            else:
                fid.write(self.hcb.encode())
                fid.seek(int(self.hcb.data_start))

            if round(self.hcb.type_code,-3) in (1000, 2000):

                # Reorder data if complex
                if self.hcb.format[0] == 'C':
                    if mach_fmt[self.hcb.data_rep] == self.data.dtype.byteorder.replace('=', mach_fmt[native_format[sys.byteorder]]):
                        fid.write(np.array((np.real(self.data), np.imag(self.data))).T.reshape((1, -1)))
                    else:
                        fid.write(np.array((np.real(self.data), np.imag(self.data))).T.reshape((1, -1)).byteswap().newbyteorder())
                else:
                    if mach_fmt[self.hcb.data_rep] == self.data.dtype.byteorder.replace('=', mach_fmt[native_format[sys.byteorder]]):
                        fid.write(self.data.T.reshape((1, -1)))
                    else:
                        fid.write(self.data.T.reshape((1, -1)).byteswap().newbyteorder())

            elif round(self.hcb.type_code,-3) in (3000, 6000):

                # For string fields
                for i in self.data.columns[self.data.dtypes == object]:
                    for tag in self.data[i].unique():
                        self.data[i].replace(to_replace=tag, value=self.tags[i].tags[self.tags[i].get_tag_index(tag)]['value'], inplace=True)

                # Write data from pandas data frame
                rec = self.data.to_records(index=False)
                rec_dtype = {'names': [i[0] for i in rec.dtype.descr],
                             'formats': [i[1] if 'O' not in i[1] else '|S8' for i in rec.dtype.descr]}
                fid.write(rec.astype(rec_dtype).tobytes())

                # For string fields
                for i in self.data.columns[self.data.dtypes == object]:
                    for tag in self.data[i].unique():
                        self.data[i].replace(to_replace=tag, value=self.tags[i].tags[self.tags[i].get_tag_index(tag)]['name'], inplace=True)

            # Write extended header block
            if append:
                fid.seek(int(base_file.hcb.ext_start*512))
                fid.write(base_file.ehb.encode())
            else:
                fid.seek(int(self.hcb.ext_start*512))
                fid.write(self.ehb.encode())


    '''
    Helper Functions
    '''

    def update_header(self, append_data_len=None):
        '''
        Updates HCB and EHB to reflect current data in file
        This should be run after adding or removing data from data set and
        before writing file to ensure accurate information

        Parameters
        ----------
        append_data_len: int
            number of samples being appended to file

        '''

        # Prepare data as appropriate and update appropriate fields
        if self.data is not None and isinstance(self.data, (pd.DataFrame, pd.Series)):

            self.hcb.type_code = 6003

            # remove any subrecords from Record that dont exist in the data
            for irec in self.subrec_names:
                if irec not in self.data.columns:
                    self.subrecs.remove(self.subrecs[self.subrec_names.index(irec)])

            # add any subrecords that dont exist in Record but are present in data
            for icol in self.data.columns:
                if icol not in self.subrec_names:
                    self.subrecs.append(Subrecord({'data': self.data[icol], 'units': int(31)}))

            # encode subrecords for HCB and EHB
            self.hcb.subrecs = bytes()
            self.ehb.keywords['SUBREC_DEF'] = ''
            offset = 0
            for i_key in self.data:
                self.hcb.subrecs = bytes().join([self.hcb.subrecs,
                                                 Subrecord.encode({'data': self.data[i_key],
                                                                   'offset': offset,
                                                                   'units': self.subrec_units[self.subrec_names.index(i_key)]},
                                                                  codec=3000)])
                self.ehb.keywords['SUBREC_DEF'] = ''.join([self.ehb.keywords['SUBREC_DEF'],
                                                           Subrecord.encode({'data': self.data[i_key],
                                                                             'offset': offset,
                                                                             'units': self.subrec_units[self.subrec_names.index(i_key)]},
                                                                            codec=6000)])
                if plat_format[self.data[i_key].dtype.char] == 'A':
                    offset += 8
                else:
                    offset += plat_type[plat_format[self.data[i_key].dtype.char]][1]

            self.ehb.keywords['SUBREC_DESCRIP'] = 'TYPE0'

            # for any string/tag fields, force units to be int(31) add ASPEN tag keywords
            self.tags = dict()
            for tag_rec in [x.name for x in self.subrecs if x.format=='SA']:
                self.tags[tag_rec] = TagSet(data=self.data[tag_rec], tag_set_string=self.get_keyword(f'{tag_rec}.TAGS'))
                self.set_keyword({f'{tag_rec}.TAGS': self.tags[tag_rec].tag_set_string})
                self.set_keyword({f'{tag_rec}_VALUES': self.tags[tag_rec].value_string})
                self.set_units(tag_rec, int(31))



            self.hcb.data_size = float(np.sum(self.data.memory_usage()[1:]))
            self.hcb.record_length = int(self.hcb.data_size//self.data.shape[0])
            self.hcb.num_subrecs = self.data.shape[1]

        elif (self.data is not None and (type(self.data) in (np.array, np.ndarray) and (len(self.data.shape) == 1 or min(self.data.shape) == 1))) or append_data_len is not None:

            if append_data_len is None:

                self.hcb.type_code = 1001
                if np.any(np.iscomplex(self.data)):
                    self.hcb.format = 'C' + plat_format[np.real(self.data[0]).dtype.char]
                else:
                    self.hcb.format = 'S' + plat_format[self.data.dtype.char]

                atom_size = np.dtype(plat_type[self.hcb.format[1]][2]).itemsize

                self.hcb.data_size = float(plat_rank[self.hcb.format[0]]*atom_size*len(self.data))

            else:
                if round(self.hcb.type_code,-3) in (3000, 6000):
                    self.hcb.data_size += float(self.element_size*append_data_len)
                else:
                    self.hcb.data_size += float(plat_rank[self.hcb.format[0]]*self.atom_size*append_data_len)

        elif self.data is not None and (type(self.data) in (np.array, np.ndarray) and (len(self.data.shape) > 1 or min(self.data.shape) > 1)):

            if np.any(np.iscomplex(self.data)):
                self.hcb.format = 'C' + plat_format[np.real(self.data[0,0]).dtype.char]
            else:
                self.hcb.format = 'S' + plat_format[self.data[0,0].dtype.char]

            self.hcb.type_code = 2000
            self.hcb.frame_length = self.data.shape[0]

            atom_size = np.dtype(plat_type[self.hcb.format[1]][2]).itemsize
            self.hcb.data_size = float(plat_rank[self.hcb.format[0]]*atom_size*np.prod(self.data.shape))

        # Updated extended header block size
        if append_data_len is None:
            self.hcb.ext_size = len(self.ehb.encode())

        # If no extended header, set size and start to 0
        if self.hcb.ext_size == 0:
            self.hcb.ext_start = 0
        else:
            self.hcb.ext_start = int(np.ceil((self.hcb.data_start + self.hcb.data_size)/512))

    def update_ehb(self):
        '''
        Updates EHB to reflect any changes in file
        '''

        # Prepare data as appropriate and update appropriate fields
        if self.data is not None and isinstance(self.data, (pd.DataFrame, pd.Series)):

#            # loop over columns in data and remove any that dont exist in record
#            data_cols = self.data.columns
#            for i in self.subrecs:
#                if i.name not in data_cols:
#                    self.subrecs.remove(i)

            # remove any subrecords from Record that dont exist in the data
            for irec in self.subrec_names:
                if irec not in self.data.columns:
                    self.subrecs.remove(self.subrecs[self.subrec_names.index(irec)])

            # add any subrecords that dont exist in Record but are present in data
            for icol in self.data.columns:
                if icol not in self.subrec_names:
                    self.subrecs.append(Subrecord({'data': self.data[icol], 'units': int(31)}))

            self.ehb.keywords['SUBREC_DEF'] = ''
            offset = 0
            for i_key in self.data.columns:
                self.ehb.keywords['SUBREC_DEF'] = ''.join([self.ehb.keywords['SUBREC_DEF'],
                                                           Subrecord.encode({'data': self.data[i_key],
                                                                             'offset': offset,
                                                                             'units': self.subrec_units[self.subrec_names.index(i_key)]},
                                                                            codec=6000)])
                if plat_format[self.data[i_key].dtype.char] == 'A':
                    offset += 8
                else:
                    offset += plat_type[plat_format[self.data[i_key].dtype.char]][1]

            self.ehb.keywords['SUBREC_DESCRIP'] = 'TYPE0'

        # Updated extended header block size
        self.hcb.ext_size = len(self.ehb.encode())

        # If no extended header, set size and start to 0
        if self.hcb.ext_size == 0:
            self.hcb.ext_start = 0
        else:
            self.hcb.ext_start = int(np.ceil((self.hcb.data_start + self.hcb.data_size)/512))

    def add_subr(self, data, units=int(31), name=None):
        '''
        Safely adds a new field to data in PlatFile object
        Accepts adding a pd.DataFrame, pd.Series

        Parameters
        ----------
        data :
            Pandas Series of data to be added
        unit : string
            Units fo field being added

        '''
        # Only valid for 3000 or 6000 files
        if round(self.hcb.type_code,-3) not in (3000, 6000):
            raise ValueError('add_subr is only applicable to 3000 or 6000 type files.')

        if isinstance(data, dict):
            data = pd.DataFrame.from_dict(data)
        elif isinstance(data, np.ndarray):
            data = pd.Series(data, name=name)

        if name is not None:
            data.rename(name, inplace=True)

        # Error if field already exist
        if isinstance(data, pd.DataFrame):
            for i in data:
                if i in self.data.columns:
                    raise ValueError(f'{i.name} already exist in data. Must have unique field names')
        elif isinstance(data, pd.Series):
            if data.name in self.data.columns:
                raise ValueError(f'{data.name} already exist in data. Must have unique field names')

        self.data = pd.concat([self.data, data], axis=1)

        if isinstance(units, str):
            units = [plat_units[units], ]
        elif isinstance(units, int):
            units = [units, ]
        elif isinstance(units, list):
            units = [x if isinstance(x, int) else plat_units[x] for x in units]

        if isinstance(data, pd.DataFrame):
            for i in enumerate(data):
                self.subrecs.append(Subrecord({'data': data[i[1]], 'units': units[i[0]]}))
        elif isinstance(data, pd.Series):
            self.subrecs.append(Subrecord({'data': data, 'units': units[0]}))


        # Update header
        self.update_header()

    def rm_subr(self, subr_name):
        '''
        Safely removes a field to data in PlatFile object

        Parameter
        ---------
        name : str
            name of field to be removed
        '''
        if isinstance(subr_name, str):
            subr_name = [subr_name, ]
        self.data = self.data.drop(subr_name, axis=1)

        # Update header
        for i in subr_name:
            self.subrecs.remove(self.subrecs[self.subrec_names.index(i)])
        self.update_header()

    def get_keyword(self, key, neg_rtn=None):
        '''
        Safely returns a keywords from the either the extended header or HCB

        Parameters
        ----------
        key: str
            key to return value of
        neg_rnt : default None
            value to return if key not found
        '''
        return self.ehb.keywords.get(key, self.hcb.keywords.get(key, neg_rtn))

    def set_keyword(self, keyword_dict):
        '''
        Safely sets a keywords from the either the extended header

        Parameters
        ----------
        keyword_dict: dict
            dictionary of keywords and values
        '''
        for x in keyword_dict.items():
            self.ehb.keywords[x[0]] = x[1]

        # Update header
        self.update_ehb()

    def get_units(self, subr_name):
        '''
        Returns units of subr_name

        Parameters
        ----------
        subr_name: str
            name of data field to get units of
        '''

        try:
            i = self.subrec_names.index(subr_name)

            # ensure subr_name is also in current data, or remove record
            if isinstance(self.data, list) or subr_name in self.data.columns:
                return plat_units[self.subrec_units[i]]

            else:
                self.subrecs.remove(self.subrecs[self.subrec_names.index(subr_name)])
                raise ValueError(f'{subr_name} does not exist in data')
        except ValueError:
            if subr_name in self.data.columns:
                return plat_units[31]
            else:
                raise ValueError(f'{subr_name} does not exist in data')

    def set_units(self, subr_name, units):
        '''
        Sets units of subr_name to units

        Parameters
        ----------
        subr_name: str
            name of data field to get units of
        units: str or int
            str representation of units or if int, Platinum unit code
        '''
        try:
            i = self.subrec_names.index(subr_name)

            # ensure subr_name is also in current data, or remove record
            if isinstance(self.data, pd.DataFrame) and subr_name not in self.data.columns:
                self.subrecs.remove(self.subrecs[self.subrec_names.index(subr_name)])
                raise ValueError(f'{subr_name} does not exist in data')

        # if field does not exist in current subrecord definitions
        except ValueError:

            # if exists in data, add new subrc
            if subr_name in self.data.columns:
                self.subrecs.append(Subrecord({'data': self.data[subr_name], 'units': units}))
                i = self.subrec_names.index(subr_name)
            else:
                raise ValueError(f'{subr_name} does not exist in data')

        if isinstance(units, str):
            units = plat_units[units]

        self.subrecs[i].units = units

    def append_data(self, data, ignore_index=True):
        '''
        Safely appends data to existing data

        Parameters
        ----------
        data: np.ndarray, pd.DataFrame, pd.Series
            data to append
        ignore_index : bool (default True)
            if a pd.Series or pd.DataFrame, whether to ignore associated index
        '''
        if round(self.hcb.type_code,-3) in (3000, 6000):
            self.data = self.data.append(data, ignore_index=ignore_index)
        else:
            self.data = np.append(self.data, data)

        # Update header
        self.update_header()


    def set_acqdatetime(self, dt=None, date=None, time=None):

        if dt is not None:
            if str(type(dt)).find('pandas') >= 0:
                dt = dt.to_pydatetime()

            if isinstance(dt, datetime.datetime):
                dt_date = datetime.date(dt.year, dt.month, dt.day)
                dt_time = datetime.time(dt.hour, dt.minute, dt.second)
            elif isinstance(dt, datetime.date):
                dt_date = dt
                acqtime = self.get_keyword('ACQTIME', '00:00:00').split(':')
                dt_time = datetime.time(int(acqtime[0]), int(acqtime[1]), int(acqtime[2].split('.')[0]))
            elif isinstance(dt, datetime.time):
                dt_time = dt
                acqdate = self.get_keyword('ACQDATE')
                dt_date = datetime.date(int(acqdate[0:4]), int(acqdate[4:6]), int(acqdate[6:].split('.')[0]))

            if isinstance(dt, (datetime.date, datetime.time)):
                dt = datetime.datetime(dt_date.year, dt_date.month, dt_date.day, dt_time.hour, dt_time.minute, dt_time.second)

        elif dt is None:
            if date is not None and isinstance(date, str) and len(date) == 8:
                dt_date = datetime.date(int(date[0:4]), int(date[4:6]), int(date[6:]))
            elif date is not None and isinstance(date, (datetime.datetime, datetime.date)):
                dt_date = datetime.date(date.year, date.month, date.day)
            else:
                acqdate = self.get_keyword('ACQDATE')
                dt_date = datetime.date(int(acqdate[0:4]), int(acqdate[4:6]), int(acqdate[6:]))

            if time is not None and isinstance(time, str) and len(time) == 8:
                acqtime = time.split(':')
                dt_time = datetime.time(int(acqtime[0]), int(acqtime[1]), int(acqtime[2]))
            elif time is not None and isinstance(time, (datetime.datetime, datetime.time)):
                dt_time = datetime.time(time.hour, time.minute, time.second)
            else:
                acqtime = self.get_keyword('ACQTIME', '00:00:00').split(':')
                dt_time = datetime.time(int(acqtime[0]), int(acqtime[1]), int(acqtime[2].split('.')[0]))

            dt = datetime.datetime(dt_date.year, dt_date.month, dt_date.day, dt_time.hour, dt_time.minute, dt_time.second)

        self.set_keyword({'ACQDATE': dt.strftime('%Y%m%d')})
        self.set_keyword({'ACQTIME': dt.strftime('%H:%M:%S')})
        self.hcb.timecode = (pd.to_datetime(dt) - pd.to_datetime(datetime.datetime(1950, 1, 1, 0, 0, 0)))//pd.to_timedelta(1, unit='s')

        if (round(self.hcb.type_code, -3) in (3000, 6000) and ('TOA' in self.data and isinstance(self.data['TOA'][0], (np.float, np.float32, np.float64, float)))) or \
           round(self.hcb.type_code, -3) in (1000, 2000):
            self.set_keyword({'TIME_EPOCH': f'{dt.strftime("%Y-%m-%d")}T{dt.strftime("%H:%M:%S")}Z'})


    '''
    Conversion / Calculation Utilites
    '''
    @property
    def percent_read(self):
        '''
        Percentage of file currently read from beginning of data
        '''
        if self.hcb.type_code == 1004:
            return self.data_element_tail/self.assoc6000.num_elements*100
        elif round(self.hcb.type_code,-3) == 2000:
            return self.data_element_tail/(self.num_elements//self.hcb.frame_length)*100
        else:
            return self.data_element_tail/self.num_elements*100

    # Collection properties
    @property
    def duration(self):
        duration = self.get_keyword('EVENT.DURATION',-1)
        if isinstance(duration, str):
            duration = float(duration)
        return np.sum(duration).astype(float)

    @property
    def start_time(self):
        if self.hcb.type_code == 1004:
            return dateutil.parser.parse(f'{self.assoc6000.get_keyword("ACQDATE")}T{self.assoc6000.get_keyword("ACQTIME")}') + datetime.timedelta(seconds=self.assoc6000.data.iloc[0]['TOA']*self.assoc6000.hcb.tag_delta)
        elif self.is_martes:
#            return dateutil.parser.parse(f'{self.get_keyword("ACQDATE")}T{self.get_keyword("ACQTIME")}') + datetime.timedelta(seconds=self.time_start)
            return dateutil.parser.parse(f'{self.ehb.keywords["ACQDATE"]}T{self.ehb.keywords["ACQTIME"]}')
        else:
            return datetime.datetime(1950, 1, 1, 0, 0, 0) + datetime.timedelta(seconds=self.hcb.timecode)

    @property
    def end_time(self):
        if self.hcb.type_code == 1004:
            return self.start_time + datetime.timedelta(seconds=(self.assoc6000.data.iloc[-1]['TOA']+self.assoc6000.data.iloc[-1]['NUMBER_SAMPLES']-self.assoc6000.data.iloc[0]['TOA'])*self.assoc6000.hcb.tag_delta)
        elif self.is_martes:
            return self.start_time + datetime.timedelta(seconds=self.duration)
        else:
            return datetime.datetime(1950, 1, 1, 0, 0, 0) + datetime.timedelta(seconds=self.hcb.timecode) + datetime.timedelta(seconds=self.duration)

    @property
    def timecode(self):
        return (pd.to_datetime(self.start_time)-pd.to_datetime(datetime.datetime(1950, 1, 1, 0, 0, 0)))/pd.to_timedelta(1, unit='s') + self.hcb.x_start

    @property
    def fs(self):
        try:
            return 1/self.hcb.x_delta
        except AttributeError:
            raise AttributeError('Fs not defined for type ' + str(self.hcb.type_code) + ' files')

    @property
    def is_martes(self):
        return self.ehb.keyword_format.lower() == 'ascii'

    @property
    def subrec_names(self):
        '''
        Returns the names of currently defined subrecords
        '''
        return [i.name for i in self.subrecs]

    @property
    def subrec_offsets(self):
        '''
        Returns the byte offsets of currently defined subrecords
        '''
        return [i.offset for i in self.subrecs]

    @property
    def subrec_units(self):
        '''
        Returns the units of currently defined subrecords
        '''
        return [i.units for i in self.subrecs]

    @property
    def subrec_formats(self):
        '''
        Returns the data formts of currently defined subrecords
        '''
        return [i.format for i in self.subrecs]

    @property
    def num_subrec(self):
        '''
        Returns the number of currently defined subrecords
        '''
        return len(self.subrecs)

    def get_events(self, keyword=None):

        # Get event keywords for desired event type
        if keyword is None:
            events_keys = [key for key in self.ehb.keywords.keys() if key.endswith('.EVENT')]
            if not len(events_keys):
                return None
        else:
            events_keys = [f'{keyword}.EVENT', ]
            if not events_keys[0] in self.ehb.keywords:
                return None

        # Parse event data
        events = {}
        for i in events_keys:
            key = i[:i.find('.EVENT')]
            events[key] = {'values': np.array(self.ehb.keywords[key]),
                           'events': np.array(self.ehb.keywords[i]),
                           'duration': np.array([self.ehb.keywords['EVENT.DURATION'][j] for j in self.ehb.keywords[i]]),
                           'offset': np.array([self.ehb.keywords['EVENT.OFFSET'][j] for j in self.ehb.keywords[i]]),
                           'time': np.array([self.ehb.keywords['EVENT.TIME'][j] for j in self.ehb.keywords[i]]),
                           'time_units': self.ehb.keywords.get('EVENT.TIME.UNITS', self.ehb.keywords.get('EVENT.UNITS'))}

        if keyword is not None:
            events = events[keyword]

        return events

    @staticmethod
    def is_plat(filename):
        """Determine whether input file is BLUE / Platinum or not.

        Parameters
        ----------
        filename : string
            path to file

        Returns
        -------
        bool

        """

        # Open file and read first 4 bytes
        f = open(filename, 'rb')
        try:
            file_type = f.read(4).decode()
        except:
            file_type = ''
        finally:
            f.close()

        # see if valid BLUE file
        if (file_type != 'BLUE') and (file_type != 'GOLD'):
            return False
        else:
            return True

    @staticmethod
    def is_valid_plat(filename):
        """Determine if a Platinum file is valid.

        Must meed following criteria:
            1. Both the HCB and EHB can be read sucessfully
               This ensures the beginning and end of file is present
            2. Calcualted file size and actual file size agree or actual size is within 512 bytes of predicted
        """
        try:
            f = PlatFile(filename, read_data=False)
            expected_file_size = f.hcb.ext_start*512 + f.hcb.ext_size
            actual_file_size = os.stat(filename).st_size
            if actual_file_size-expected_file_size < 512:
                return True
            else:
                return False
        except:
            return False

'''
PLATINUM File Helper Functions
'''

def get_plat_units_of_data(name, data):
    '''
    Function returns best guess of units for given field name / data
    '''
    name = name.upper()
    if name in ('TOA', ) and isinstance(data, (np.float, np.float32, np.float64, float)):
        return plat_units['s']
    elif name in ('TOA', ) and isinstance(data, (np.int64, int)):
        return plat_units['Counts']
    elif name in ('PF', 'MOD_PARAMETER', 'PF_DEV', 'RF', 'FREQ_RF', 'FC', 'PRF', 'LFM', 'BW'):
        return plat_units['Hz']
    elif name in ('PA', 'SNR', 'PA_DEV'):
        return plat_units['dB']
    elif name in ('MOD_TYPE', 'AOA_TYPE', 'NUM_CHIPS', 'CHANNEL', 'FLAGS', 'DEINT_ID', 'NUM_STATES', 'BAND_STATE', 'PSK_SEQ', 'SPARE'):
        return plat_units['None']
    elif name in ('AOA', 'PHASE', 'AOA_DEV', 'PHASE_DEV'):
        return plat_units['rad']
    elif name in ('PW', 'TOA_DEV', 'PHASE_REF_TIME', 'PW_DEV'):
        return plat_units['s']
    elif name in ('MOD_PARAMETER_DEV', 'CHIRP_QUAD', 'CHIRP_CUBIC', 'CHIRP_QUAD_DEV', 'CHIRP_CUBIC_DEV'):
        return 9 # Hz/s
    elif name.find('POS') > 0 or name.find('RNG') > 0 or name.find('ALT') > 0:
        return plat_units['m']
    elif name in ('VEL', 'VELX', 'VELY', 'VELZ', 'SPD'):
        return plat_units['m/s']
    elif name in ('ACC', 'ACCEL', 'ACCX', 'ACCELX', 'ACCY', 'ACCELY', 'ACCZ', 'ACCELZ') > 0:
        return plat_units['m/s^2']
    elif name in ('AZ', 'EL', 'LON', 'LAT', 'LATITUDE', 'LONGITUDE'):
        return plat_units['deg']
    else:
        return plat_units['None']


'''
PLATINUM File Helper Data tyes and Enumerations
'''
# This definition matches the format code with the data format
plat_type = {'B': ['b', 1, np.int8],    # 1 byte signed int
             'O': ['B', 1, np.uint8],   # 1 byte unsigned int
             'I': ['h', 2, np.int16],   # 2 byte signed int
             'U': ['H', 2, np.uint16],  # 2 byte unsigned int
             'L': ['i', 4, np.int32],   # 4 byte signed int
             'V': ['I', 4, np.uint32],  # 4 byte unsigned int
             'X': ['q', 8, np.int64],   # 8 byte signed long long.
             'F': ['f', 4, np.single],  # 4 byte float
             'D': ['d', 8, float],      # 8 byte float
             'A': ['s', 1, str]}        # 1 byte char


# Platinum Formats
# Dictionary defining Platinum rank code
# The one-character rank codedesignates the number of scalars grouped together
plat_rank = {'S': 1,   1: 'S',  # Scalar, 1 element per data point
             'C': 2,   2: 'C',  # Complex, 2 elements (real, imaginary) per data point
             'V': 3,   3: 'V',  # Vector, 3 elements (x,y,z) per data point
             'Q': 4,   4: 'Q',  # Quad, 4 elements (x,y,z,time) per data point
             'M': 9,   9: 'M',  # Matrix, 3-by-3 (coordinate transformation, 9 elements per data point
             'X': 10, 10: 'X',  # 10 elements (Xmidas defined)
             'A': 10,           # 10 elements (other defined)
             'T': 16, 16: 'T',  # Transform Matrix, 4-by-4 (coordinate transformation and scaling), 16 elements per data point
             'U': 1}  # User Defined, not recommended, very little library support

plat_format = {'b': 'B',  # 1 byte signed int
               'B': 'O',  # 1 byte unsigned int
               'h': 'I',  # 2 byte signed int
               'H': 'U',  # 2 byte unsigned int
               'i': 'L',  # 4 byte signed int
               'I': 'V',  # 4 byte unsigned int
               'l': 'X',  # 8 byte signed int
               'f': 'F',  # 4 byte float
               'd': 'D',  # 8 byte float
               'U': 'A',  # 1 char
               'S': 'A',  # 1 char
               'O': 'A',  # 1 char
               'M': 'X',  # If specifying time in a datetime64 (int64) format
               'm': 'X'}  # If specifying time in a datetime64 (int64) format

# This definition matches the unit code with the units
plat_units = {0: 'N/A', 'N/A': 0,  # not applicable
              1: 's', 's': 1,  # time in seconds
              2: 's',  # delay in seconds
              3: 'Hz', 'Hz': 3,  # frequency in hertz
              4: 'Time Code', 'Time Code': 4,  # time code
              5: 'm', 'm': 5,   # distance in meters
              6: 'm/s', 'm/s': 6,  # velocity
              7: 'm/s^2', 'm/s^2': 7,  # acceleration
              8: 'm/s^3', 'm/s^3': 8,  # jerk
              9: 'Hz',  # doppler
              10: 'Hz/s', 'Hz/s': 10,  # doppler rate
              11: 'J', 'J': 11,  # energy in joules
              12: 'W', 'W': 12,  # power in watts
              13: 'g', 'g': 13,  # mass in grams
              14: 'L', 'L': 14,  # volume in liters
              15: 'W/sr', 'W/sr': 15,  # power radiated per solid angle in watts per steradian
              16: 'W/rad', 'W/rad': 16,  # power radiated per angle in watts per radian
              17: 'W/m^2', 'W/m^2': 17,  # power radiated through a surface in watts per square meter
              18: 'W/m', 'W/m': 18,  # power radiated in watts per meter
              19: 'W/MHz', 'W/MHz': 19,  # radiated power per frequency range in watts per megahertz
              30: 'Unk', 'Unk': 30,  # unknown units
              31: 'None', 'None': 31,  # dimensionless number
              32: 'Counts', 'Counts': 32,  # counts, often stored as integer counts
              33: 'rad', 'rad': 33,  # angle or phase in radians
              34: 'deg', 'deg': 34,  # angle or phase in degrees
              35: 'dB', 'dB': 35,  # ratio in decibels
              36: 'dBm', 'dBm': 36,  # power ratio in decibels relative to one milliwatt
              37: 'dBW', 'dBW': 37,  # power ratio in decibels relative to one watt
              38: 'sr', 'sr': 38,  # solid angle (steradians)
              40: 'ft', 'ft': 40,  # distance in feet
              41: 'nautical miles', 'nautical miles': 41,  # distance in nautical miles
              42: 'ft/s', 'ft/s': 42,  # velocity in feet per second
              43: 'nautical miles/s', 'nautical miles/s': 43,  # velocity in nautical miles per second
              44: 'kt', 'kt': 44,  # velocity in knots
              45: 'ft/s^2', 'ft/s^2': 45,  # acceleration in feet per second squared
              46: 'kt/hr', 'kt/hr': 46,  # acceleration in knots per hour
              47: 'kt/s', 'kt/s': 47,  # acceleration in knots per second
              48: 'gravities', 'gravities': 48,  # acceleration as gravities or g-force
              49: 'grav/s', 'grav/s': 49,  # jerk in gravities per second
              50: 'Hz',  # angular velocity in revolutions per second
              51: 'rpm', 'rpm': 51,  # angular velocity in revolutions per minute
              52: 'rad/s', 'rad/s': 52,  # angular velocity in radians per second
              53: 'deg/s', 'deg/s': 53,  # angular velocity in degrees per second
              54: 'rad/s^2', 'rad/s^2': 54,  # angular acceleration in radians per second squared
              55: 'deg/s^2', 'deg/s^2': 55,  # angular acceleration in degrees per second squared
              56: '%', '%': 56,  # % percentage
              57: 'psi', 'psi': 57,  # pressure in pounds per square inch
              64: 'Hz/s^2',  #
              65: 'Hz/s^3', 'Hz/s^3': 65}  #

plat_boolean_enums = {0: False, False: 0,
                      1: True, True: 1}

plat_coherency_enums = {-1: 'INVALID', 'INVALID': -1,
                        0: 'NON_COHERENT', 'NON_COHERENT': 0,
                        1: 'COHERENT', 'COHERENT': 1,
                        2: 'MAYBE_COHERENT', 'MAYBE_COHERENT': 2}

plat_polarization_enums = {-1: 'INVALID', 'INVALID': -1,
                           0: 'RHC', 'RHC': 0,
                           1: 'LHC', 'LHC': 1,
                           2: 'NORMAL_RHC', 'NORMAL_RHC': 2,
                           3: 'NORMAL_LHC', 'NORMAL_LHC': 3,
                           4: 'CROSS_RHC', 'CROSS_RHC': 4,
                           5: 'CROSS_LHC', 'CROSS_LHC': 5,
                           6: 'HORIZONTAL', 'HORIZONTAL': 6,
                           7: 'VERTICAL', 'VERTICAL': 7,
                           8: 'LEFT_RIGHT', 'LEFT_RIGHT': 8,
                           9: 'NO_POLARIZATION', 'NO_POLARIZATION': 9,
                           10: 'UNKNOWN', 'UNKNOWN': 10}

plat_spectral_sense_enums = {-1: 'INVALID', 'INVALID': -1,
                             0: 'UPRIGHT', 'UPRIGHT': 0,
                             1: 'INVERTED', 'INVERTED': 1,
                             2: 'NA', 'NA': 2}

plat_scaling_enums = {-1: 'INVALID', 'INVALID': -1,
                      1: 'COLLECTOR', 'COLLECTOR': 1,
                      2: 'GROUND', 'GROUND': 2,
                      3: 'BORESITE', 'BORESITE': 3,
                      4: 'PIERCE_POINT', 'PIERCE_POINT': 4,
                      5: 'AT_AOI', 'AT_AOI': 5}

plat_mod_type = {0: 'INVALID', 'INVALID': 0,
                 1: 'UNMODULATED', 'UNMODULATED': 1,
                 2: 'LFM', 'LFM': 2,
                 3: 'NLFM', 'NLFM': 3,
                 4: 'PSK', 'PSK': 4,
                 5: 'STEPFREQ', 'STEPFREQ': 5,
                 6: 'UNKANG', 'UNKANG': 6}

aspen_color_pallette = ['#ff420e', '#004586', '#ffd320', '#579d1c', '#7e0021',
                        '#83caff', '#314004', '#aecf00', '#4b1f6f', '#ff950e',
                        '#c5000b', '#0084d1', '#ccffff', '#ccffcc', '#ffff99',
                        '#ff99cc', '#cc99ff', '#ffcc99', '#3366ff', '#33cccc',
                        '#99cc00', '#ffcc00', '#ff9900', '#ff6600', '#ff0000',
                        '#00ff00', '#0000ff', '#ffff00', '#ff00ff', '#00ffff']
