#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Classification: UNCLASSIFIED

Created on Tue Dec 11 15:04:24 2018

"""

import os
import getpass as gp
import numpy as np
import tkinter as tk
from tkinter import filedialog, messagebox
from io import PlatFile
from io.features import calc_rf, calc_dtoa, calc_bw


def addparam(filename=None):
    """
    This function is designed to take .epdw and .sdw files as an input and
    output a new file with some new parameters. Output files will be placed
    in the same directory that was given in the input.

    This function can be called with or without an input argument. This program
    expects a path/file string as the input argument. If no input argument is
    given, a file dialog box will open allowing you to choose one or multiple
    platinum files.

    When passing files to addparam use the format below:
         addparam(['string pathname/filename'])

    Current Parameters added:
        PDW:            SDW:
            FREQ_RF         FREQ_RF
            LFM             PRI
            TARGET_ID       TOA
            MODE            BURST_DUR
            WAVEFORM        DWELL_DUR
                            TARGET_ID
                            MODE
                            WAVEFORM
    Created VER 1.0: 20181211
    Update, VER 1.1: 20191025 - Addparam now adds a PRI field to .sdw files
    Update, VER 2.0: 20191029 - New parameters added to files, including TOA
                                  BURST_DUR, DWELL_DUR, MODE, and WAVEFORM
                                  Fixed bug where units were not set to new
                                  parameters. Added version check that will
                                  overwrite old AP files with the current
                                  version. Fixed bug that prevented tag fields
                                  from being edited in Aspen.
    Update, VER 2.1: 20191030 - Addparam now adds TARGET_ID tag field similar
                                  to MODE and WAVEFORM tag fields with all
                                  points 'UNTAGGED'
    Update, VER 2.2: 20191104 - Output filename is now a .mpdw or .msdw instead
                                  of _AP format
    Update, VER 2.2.1: 20191120 - Fixed bug in msdw files where time field will
                                       improperly display in HMS
    Update, VER 2.2.2: 20191121 - Fixed bug where BURST_DUR was calculated
                                       incorrectly. Fixed bug where files
                                       did not overwrite correctly. Now will
                                       remove files with _AP in the filename
    Update, VER 2.2.3: 20191202 - Updated code to reflect changes in PlatFile
                                       object and updates to msic3a_io
    Update, VER 2.2.4: 20191218 - LFM field changed to BW field and multiple
                                       modulation types accounted for
    Update. VER 2.2.5: 20200131 - Changed FREQ_RF to RF for future runs. Edited
                                       how calc_bw handles UNMOD pulses.
    Update, VER 2.2.6: 20200205 - Now removes FREQ_RF and replaces with RF
    Update, VER 2.2.7: 20200214 - Overwrites all created fields if version is not current

    """

    CUR_VER = '2.2.7'  # this needs changed in both addparam.py and addparam.m when updated
    if filename is None:
        # open filedialog box
        root = tk.Tk()
        root.withdraw()
        filename = filedialog.askopenfilenames(
                  initialdir='/home/' + gp.getuser(),
                  filetypes=[('Platinum Files', ('*.epdw', '*.sdw', '*.mpdw', '*.msdw')),
                             ('All Files', '*')],
                  parent=root, title='Pick Multiple Files')
    if len(filename) == 0:
        messagebox.showerror('Error',
                             'No files were chosen. AddParam will now close!')
        return
    else:
        if isinstance(filename, str):
            filename = [filename, ]

        for f in filename:
            # Read in data
            print('Reading data for', f)
            input_file = PlatFile(f)
            # input_file.read_data()  # only need if reading CDIF
            # Create FREQ_RF and LFM fields
            if input_file.get_keyword('SUBREC_DEF_NAME', 0).startswith('SDW_'):
                fname = '.msdw'  # used later for creating new filename
                if 'FREQ_RF' in input_file.subrec_names:
                    print('Replacing FREQ_RF field with RF field...')
                    # input_file.rm_subr(input_file, name='FREQ_RF')
                    input_file.rm_subr(subr_name='FREQ_RF')
                if 'RF' in input_file.subrec_names:
                    print('RF field already exists.')
                else:
                    print('Generating RF field now...')
                    input_file.add_subr(calc_rf(input_file, IF='FC'), units='Hz', name='RF')
                    # input_file.set_units('RF', 'Hz') # not sure why I put this here, but I don't think I need it
                if 'PRI' in input_file.subrec_names:
                    print('PRI field already exists.')
                else:
                    print('Generating PRI field now...')
                    input_file.add_subr(1 / input_file.data['PRF'], units='s', name='PRI')
                if 'TOA' in input_file.subrec_names:
                    print('TOA field already exists.')
                else:
                    print('Generating TOA field now...')
                    input_file.add_subr(input_file.data['FIRST_TOA'], units='Counts', name='TOA')
                if 'BURST_DUR' in input_file.subrec_names:
                    print('BURST_DUR field already exists.')
                else:
                    # create burst duration field and convert to seconds
                    print('Generating BURST_DUR field now...')
                    input_file.add_subr((input_file.data['LAST_TOA']-input_file.data['FIRST_TOA'])/1e10, units='s', name='BURST_DUR')
                if 'DWELL_DUR' in input_file.subrec_names:
                    print('DWELL_DUR field already exists.')
                else:
                    # create dwell duration field
                    print('Generating DWELL_DUR field now...')
                    # input_file.add_subr(data=input_file.calc_dtoa(time=input_file.data['FIRST_TOA'], units='s', name='DWELL_DUR'))
                    input_file.add_subr(calc_dtoa(input_file, time='FIRST_TOA'), units='s', name='DWELL_DUR')
                if 'TARGET_ID' in input_file.subrec_names:
                    print('TARGET_ID field already exists.')
                else:
                    print('Generating TARGET_ID field now...')
#                    input_file.add_subr(np.ones(input_file.num_elements, dtype=np.chararray)*'UNTAGGED', name='TARGET_ID')
                    input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements).astype(np.chararray), name='TARGET_ID')
                    input_file.hcb.protected = 0
                if 'MODE' in input_file.subrec_names:
                    print('MODE field already exists.')
                else:
                    print('Generating MODE field now...')
#                    input_file.add_subr(np.ones(input_file.num_elements, dtype=np.chararray)*'UNTAGGED', name='MODE')
                    input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements).astype(np.chararray), name='MODE')
                    input_file.hcb.protected = 0
                if 'WAVEFORM' in input_file.subrec_names:
                    print('WAVEFORM field already exists.')
                else:
                    print('Generating WAVEFORM field now...')
#                    input_file.add_subr(np.ones(input_file.num_elements, dtype=np.chararray)*'UNTAGGED', name='WAVEFORM')
                    input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements).astype(np.chararray), name='WAVEFORM')
            elif input_file.get_keyword('SUBREC_DEF_NAME').startswith('PDW_'):
                fname = '.mpdw'  # used later for creating new filename
                if 'FREQ_RF' in input_file.subrec_names:
                    print('Replacing FREQ_RF field with RF field...')
                    # input_file.rm_subr(input_file, subr_name='FREQ_RF')
                    input_file.rm_subr(subr_name='FREQ_RF')
                if 'RF' in input_file.subrec_names:
                    print('RF field already exists.')
                else:
                    print('Generating RF field now...')
                    input_file.add_subr(calc_rf(input_file, IF='PF'), units='Hz', name='RF')
                if 'LFM' in input_file.subrec_names:
                    print('Replacing LFM field with BW field...')
                    input_file.rm_subr(subr_name='LFM')
                if 'BW' in input_file.subrec_names:
                    print('BW field already exists.')
                else:
                    print('Generating BW field now...')
                    input_file.add_subr(calc_bw(input_file), units='Hz', name='BW')
                if 'TARGET_ID' in input_file.subrec_names:
                    print('TARGET_ID field already exists.')
                else:
                    print('Generating TARGET_ID field now...')
                    # input_file.add_subr(np.ones(input_file.num_elements, dtype=np.chararray)*'UNTAGGED', name='TARGET_ID')
                    input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements).astype(np.chararray), name='TARGET_ID')
                    # input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements), name='TARGET_ID')
                    input_file.hcb.protected = 0
                if 'MODE' in input_file.subrec_names:
                    print('MODE field already exists.')
                else:
                    print('Generating MODE field now...')
                    # input_file.add_subr(np.ones(input_file.num_elements, dtype=np.chararray)*'UNTAGGED', name='MODE')
                    input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements).astype(np.chararray), name='MODE')
                    input_file.hcb.protected = 0
                if 'WAVEFORM' in input_file.subrec_names:
                    print('WAVEFORM field already exists.')
                else:
                    print('Generating WAVEFORM field now...')
                    # input_file.add_subr(np.ones(input_file.num_elements, dtype=np.chararray)*'UNTAGGED', name='WAVEFORM')
                    input_file.add_subr(np.repeat('UNTAGGED', input_file.num_elements).astype(np.chararray), name='WAVEFORM')
            else:
                messagebox.showerror('Error',
                                     'AddParam does not recognise this file' +
                                     'type and will now close!')
                return

            # Version Check / Write new file
            if '.m' in f:
                if input_file.get_keyword('AP_VER') != CUR_VER:
                    outfile = (f.split('.')[0] + fname)
                    input_file.set_keyword({'AP_VER': CUR_VER})
                    print('\nOverwriting file with current version of AP... ')
                    input_file.write(outfile, overwrite=True)
                else:
                    print('\nDid not update file because AP version is current.')
            elif '_AP' in f:
                outfile = (f.split('_AP')[0] + fname)
                if os.path.exists(outfile):
                    print('\nDid not write new file because ' + outfile + ' already exists.\n')
                    print('Removing old version of AP...')
                    os.remove(f)
                else:
                    input_file.set_keyword({'AP_VER': CUR_VER})
                    print('\nWriting new file with current version of AP...')
                    input_file.write(outfile)
                    print('Removing old version of AP...')
                    os.remove(f)
            else:
                outfile = (f.split('.')[0] + fname)
                if os.path.exists(outfile):
                    print('\nDid not write new file because ' + outfile + ' already exists.\n')
                else:
                    input_file.set_keyword({'AP_VER': CUR_VER})
                    print('\nWriting data to new file...')
                    input_file.write(outfile)
            print(f'Done with {f} (file {filename.index(f)+1} of {len(filename)})\n')
            # print('Done with ', f, '(file ', filename.index(f)+1, 'of ', len(filename), ')\n')
    print('\nThanks for using Addparam!')


if __name__ == '__main__':

    addparam()
